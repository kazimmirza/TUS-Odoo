from odoo.http import request
from odoo import http,_,tools
import requests
import json
import base64
import phonenumbers
from odoo.exceptions import UserError, ValidationError
from phonenumbers.phonenumberutil import (
    region_code_for_country_code,
)

class WebHook(http.Controller):

    @http.route(['/check/partner/user'], auth='public', type='json', website=True, methods=['POST'])
    def check_partner_user(self, **kw):
        if kw.get('partner_id'):
            user = request.env['res.users'].sudo().search([('partner_id', '=', int(kw.get('partner_id')))])
            if user:
                return True
            else:
                return False

    @http.route(['/get/template/content'], auth='public', type='json', website=True, methods=['POST','GET'])
    def get_template_content(self, **kw):
        template_id = False
        if kw.get('template_id') != '':
            template_id = request.env['wa.template'].sudo().browse(int(kw.get('template_id')))
        pos_order_id = request.env['pos.order'].sudo().search([('pos_reference', '=', kw.get('order_name'))])
        if template_id and pos_order_id:
            body = template_id._render_field('body_html', [pos_order_id.id], compute_lang=True)[
                pos_order_id.id]
            body = tools.html2plaintext(body)
            return body
        else:
            return False

    @http.route(['/send/receipt'], auth='public', type='json', website=True, methods=['POST'])
    def send_receipt_by_whatsapp(self, **kw):
        image = kw.get('image')
        Attachment = request.env['ir.attachment']
        partner_id = request.env['res.partner'].sudo().browse(int(kw.get('id')))
        name = kw.get('receipt_name')
        filename = 'Receipt-' + name + '.jpg'
        attac_id = Attachment.sudo().search([('name', '=', filename)], limit=1)
        if len(attac_id) == 0:
            attac_id = Attachment.create({
                'name': filename,
                'type': 'binary',
                'datas': image,
                'res_model': 'wa.msgs',
                'store_fname': filename,
                'mimetype': 'image/jpeg',
            })
        user_partner = request.env.user.partner_id
        # users = request.env['res.users'].sudo().search([])
        part_lst = [user_partner.id]
        part_lst.append(partner_id.id)
        channels = request.env['mail.channel'].sudo().search([])
        channel = False
        for chn in channels:
            if partner_id.id in chn.channel_partner_ids.ids and chn.whatsapp_channel:
                channel = chn
                break
        if channel:
            channel = channel
        else:
            name = partner_id.phone
            channel = request.env['mail.channel'].create({
                'public': 'public',
                'channel_type': 'chat',
                'email_send': False,
                'name': name,
                'whatsapp_channel': True,
                'channel_partner_ids': [(6, 0, part_lst)],
            })
            channel.write({'channel_last_seen_partner_ids': [(5, 0, 0)] + [
                (0, 0, {'partner_id': line_vals}) for line_vals in part_lst]})

        if channel:
            message_values = {
                'body': kw.get('message'),
                'author_id': user_partner.id,
                'email_from': user_partner.email or '',
                'model': 'mail.channel',
                'message_type': 'wa_msgs',
                'isWaMsgs': True,
                'subtype_id': request.env['ir.model.data'].sudo().xmlid_to_res_id('mail.mt_comment'),
                'channel_ids': [(4, channel.id)],
                'partner_ids': [(4, user_partner.id)],
                'res_id': channel.id,
                'reply_to': user_partner.email,
            }
            if attac_id:
                message_values.update({'attachment_ids': [(4, attac_id.id)]})
            message = request.env['mail.message'].sudo().create(
                message_values)
            notifications = channel._channel_message_notifications(message)
            request.env['bus.bus'].sendmany(notifications)
        return True

    @http.route(['/webhook'], auth='public', type="json", csrf=False, methods=['GET', 'POST'])
    def docusign_webhook_loa(self, **kw):
        data = json.loads(request.httprequest.data.decode('utf-8'))
        company_id = request.env.user.company_id.id
        company = request.env['res.company'].sudo().browse(company_id)
        if company.authenticated:
            url = company.api_url + company.instance_id + "/me?token=" + company.api_token

            payload = {}
            headers = {}

            try:
                response = requests.request("GET", url, headers=headers, data=payload)
            except requests.exceptions.ConnectionError:
                raise UserError(
                    ("please check your internet connection."))
            if response.status_code == 200:
                dict = json.loads(response.text)

                print(dict)
                if 'id' in dict:
                    whatsapp_number = dict['id'].split('@')[0]
                    partner = False

                    partners = request.env['res.partner'].sudo().search([])
                    for part in partners:
                        if part.phone:
                            res = part.phone.strip('+')
                            number = res.replace(" ", "")
                            if number == whatsapp_number:
                                user_partner = part
                                break

                    if 'messages' in data:
                        for mes in data['messages']:
                            phone_no = mes['author'].split('@')
                            chat_number = mes['chatId'].split('@')
                            if chat_number[1] == 'c.us':
                                partners = request.env['res.partner'].sudo().search([])
                                for part in partners:
                                    if part.phone:
                                        number = part.phone.strip('+').replace(" ", "")
                                        if number == chat_number[0]:
                                            partner = part
                                            break
                                partner_id = False
                                if partner:
                                    partner_id = partner.id
                                else:
                                    pn = phonenumbers.parse('+'+chat_number[0])
                                    country_code = region_code_for_country_code(pn.country_code)
                                    country_id = request.env['res.country'].sudo().search(
                                        [('code', '=', country_code)], limit=1)
                                    partner = request.env['res.partner'].sudo().create(
                                        {'name': chat_number[0],'country_id':country_id.id,'phone': chat_number[0]})
                                    partner_id = partner.id

                                if user_partner.phone.strip('+').replace(" ", "") != phone_no[0]:
                                        if mes['type'] == 'chat':
                                            res = request.env['wa.msgs'].sudo().create({'message': mes.get('body'),
                                                                                     'phn_no': chat_number[0], 'type': 'received',
                                                                                     'partner_id':partner_id,
                                                                                        'attachment_ids':False})

                                        else:
                                            datas = base64.b64encode(requests.get(mes.get('body')).content)
                                            file_name = mes.get('body').rsplit('/', 1)[1]
                                            attachment_value = {
                                                'name': file_name,
                                                'datas': datas,
                                            }
                                            attachment = request.env['ir.attachment'].sudo().create(attachment_value)
                                            res = request.env['wa.msgs'].sudo().create({'message': "",
                                                                                        'phn_no': chat_number[0],
                                                                                        'type': 'received',
                                                                                        'partner_id': partner_id,
                                                                                        'attachment_ids':[(4,attachment.id)]})
                                else:
                                    # users = request.env['res.users'].sudo().search([])
                                    part_lst = [user_partner.id]
                                    part_lst.append(partner_id)

                                    channels = request.env['mail.channel'].sudo().search([])
                                    channel = False
                                    for chn in channels:
                                        if partner_id in chn.channel_partner_ids.ids and chn.whatsapp_channel:
                                            channel = chn
                                            break
                                    if channel:
                                        channel = channel
                                    else:
                                        name = chat_number[0]
                                        channel = request.env['mail.channel'].sudo().create({
                                            'public': 'public',
                                            'channel_type': 'chat',
                                            'email_send': False,
                                            'name': name,
                                            'whatsapp_channel': True,
                                            'channel_partner_ids': [(6, 0, part_lst)],
                                            # 'channel_last_seen_partner_ids': [(6,0,part_lst)]
                                        })
                                        channel.write({'channel_last_seen_partner_ids': [(5, 0, 0)] + [
                                            (0, 0, {'partner_id': line_vals}) for line_vals in part_lst]})
                                    if channel:
                                        message_values = {
                                            'body':  mes.get('body'),
                                            'author_id': user_partner.id,
                                            'email_from': user_partner.email or '',
                                            'model': 'mail.channel',
                                            'message_type': 'wa_msgs',
                                            'isWaMsgs': True,
                                            'subtype_id': request.env['ir.model.data'].sudo().xmlid_to_res_id('mail.mt_comment'),
                                            'channel_ids': [(4, channel.id)],
                                            'partner_ids': [(4, user_partner.id)],
                                            'res_id': channel.id,
                                            'reply_to': user_partner.email,
                                        }
                                        if mes['type'] != 'chat':
                                            datas = base64.b64encode(requests.get(mes.get('body')).content)
                                            file_name = mes.get('body').rsplit('/', 1)[1]
                                            attachment_value = {
                                                'name': file_name,
                                                'datas': datas,
                                            }
                                            attachment = request.env['ir.attachment'].sudo().create(attachment_value)

                                            if attachment:
                                                message_values.update({'body':  '','attachment_ids': [(4,attachment.id)]})

                                        if mes.get('self') == 0:
                                            message = request.env['mail.message'].sudo().with_context({'whatsapp': True}).create(
                                                message_values)
                                            notifications = channel._channel_message_notifications(message)
                                            request.env['bus.bus'].sendmany(notifications)