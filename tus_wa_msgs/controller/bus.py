# -*- coding: utf-8 -*

from odoo.addons.bus.controllers.main import BusController
from odoo.http import request


class MessageBusController(BusController):
    """
        # ------------------------------------------------------
        # Extends BUS Controller Poll to add custom notification
        # ------------------------------------------------------
    """

    def _poll(self, dbname, channels, last, options):
        if request.session.uid:
            # Channel for showing notification
            channels.append((request.db, 'incoming.Message.details', request.env.user.partner_id.id))
            # Channel for removing notification
            channels.append((request.db, 'close.incoming.Message.details', request.env.user.partner_id.id))

        return super(MessageBusController, self)._poll(dbname, channels, last, options)
