import ast
import base64
import re

from odoo import _, api, fields, models, tools
from odoo.exceptions import UserError

class WAComposer(models.TransientModel):

    _name = 'wa.compose.message'
    _description = 'Whatsapp composition wizard'

    @api.model
    def default_get(self, fields):
        result = super(WAComposer, self).default_get(fields)
        if self.env.context.get('active_model'):
            active_model = str(self.env.context.get('active_model'))
            record = self.env[active_model].browse(self.env.context.get('active_id'))
            if active_model == 'res.partner':
                result['partner_id'] = record.id
            else:
                result['partner_id'] = record.partner_id.id
            if 'report' in self.env.context:
                report = str(self.env.context.get('report'))
                report_id = self.env.ref(report).sudo()
                pdf = report_id._render_qweb_pdf(record.id)
                Attachment = self.env['ir.attachment'].sudo()
                b64_pdf = base64.b64encode(pdf[0])
                name = ((record.state in ('draft', 'sent') and _('Quotation - %s') % record.name) or
                        _('Order - %s') % record.name)
                name = '%s.pdf' % name
                attac_id = Attachment.search([('name', '=', name)], limit=1)
                if len(attac_id) == 0:
                    attac_id = Attachment.create({'name': name,
                                                  'type': 'binary',
                                                  'datas': b64_pdf,
                                                  'res_model': 'wa.msgs',
                                                  })
                result['partner_id'] = record.partner_id.id
                result['attachment_ids'] = [(4, attac_id.id)]
            return result

    def _get_current_model_template(self):
        domain=[]
        if self.env.context.get('active_model'):
            active_model = str(self.env.context.get('active_model'))
            domain = [('model','=',active_model)]
        return domain

    body = fields.Html('Contents', default='', sanitize_style=True)
    partner_id = fields.Many2one(
        'res.partner')
    template_id = fields.Many2one(
        'wa.template', 'Use template', index=True, domain=_get_current_model_template
       )
    attachment_ids = fields.Many2many(
        'ir.attachment', 'wa_compose_message_ir_attachments_rel',
        'wa_wizard_id', 'attachment_id', 'Attachments')

    @api.onchange('template_id')
    def onchange_template_id_wrapper(self):
        self.ensure_one()
        active_model = str(self.env.context.get('active_model'))
        active_record = self.env[active_model].browse(self.env.context.get('active_id'))
        for record in self:
            if record.template_id:
                record.body = record.template_id._render_field('body_html', [active_record.id], compute_lang=True)[active_record.id]
            else:
                record.body = ''

    def send_quotation_using_whatsapp(self):
        active_model = str(self.env.context.get('active_model'))
        record = self.env[active_model].browse(self.env.context.get('active_id'))
        if active_model != 'res.partner':
            record.filtered(lambda s: s.state == 'draft').write({'state': 'sent'})
        user_partner = self.env.user.partner_id
        # users = self.env['res.users'].sudo().search([])
        part_lst = [user_partner.id]
        part_lst.append(self.partner_id.id)
        channels = self.env['mail.channel'].sudo().search([])
        channel = False
        for chn in channels:
            if self.partner_id.id in chn.channel_partner_ids.ids and chn.whatsapp_channel:
                channel = chn
                break
        if channel:
            channel = channel
        else:
            name = self.partner_id.phone
            channel = self.env['mail.channel'].create({
                'public': 'public',
                'channel_type': 'chat',
                'email_send': False,
                'name': name,
                'whatsapp_channel': True,
                'channel_partner_ids': [(6, 0, part_lst)],
            })
            channel.write({'channel_last_seen_partner_ids': [(5, 0, 0)] + [
                (0, 0, {'partner_id': line_vals}) for line_vals in part_lst]})

        if channel:
            message_values = {
                'body': tools.html2plaintext(self.body),
                'author_id': user_partner.id,
                'email_from': user_partner.email or '',
                'model': 'mail.channel',
                'message_type': 'wa_msgs',
                'isWaMsgs': True,
                'subtype_id': self.env['ir.model.data'].sudo().xmlid_to_res_id('mail.mt_comment'),
                'channel_ids': [(4, channel.id)],
                'partner_ids': [(4, user_partner.id)],
                'res_id': channel.id,
                'reply_to': user_partner.email,
            }
            if self.attachment_ids:
                message_values.update({'attachment_ids': [(4, attac_id.id) for attac_id in self.attachment_ids]})
            message = self.env['mail.message'].sudo().create(
                message_values)
            notifications = channel._channel_message_notifications(message)
            self.env['bus.bus'].sendmany(notifications)

            message_values = {
                'body': self.body,
                'author_id': user_partner.id,
                'email_from': user_partner.email or '',
                'model': active_model,
                'message_type': 'comment',
                'subtype_id': self.env['ir.model.data'].sudo().xmlid_to_res_id('mail.mt_comment'),
                'channel_ids': [(4, channel.id)],
                'partner_ids': [(4, user_partner.id)],
                'res_id': record.id,
                'reply_to': user_partner.email,
            }
            if self.attachment_ids:
                message_values.update({'attachment_ids': [(4, attac_id.id) for attac_id in self.attachment_ids]})
            message = self.env['mail.message'].sudo().create(
                message_values)
            notifications = channel._channel_message_notifications(message)
            self.env['bus.bus'].sendmany(notifications)


