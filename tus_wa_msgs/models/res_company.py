from odoo import fields, models,api
from odoo.http import request
import requests
import json
import base64
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource

class Company(models.Model):
    _inherit = "res.company"

    def _get_default_image(self):
        image_path = get_module_resource('tus_wa_msgs', 'static/src/image', 'whatsapp_default_set.png')
        return base64.b64encode(open(image_path, 'rb').read())

    api_url = fields.Char(String="API URL")
    instance_id = fields.Char(String="Instance ID")
    api_token = fields.Char(String="Token")
    wa_qr_code = fields.Binary("QR Code")
    authenticated = fields.Boolean('Authenticated')
    back_image = fields.Binary(string='Background Image', help='Background image', default=_get_default_image,
                               attachment=True, )

    def get_qr_code(self):
        if self.api_token and self.instance_id and self.api_token:
            url = self.api_url+self.instance_id+"/qr_code?token="+self.api_token

            payload = {}
            headers = {}
            try:
                response = requests.request("GET", url, headers=headers, data=payload)
            except requests.exceptions.ConnectionError:
                raise UserError(
                    ("please check your internet connection."))
            if response.status_code == 200:
                data = base64.encodebytes(response.content)
                self.wa_qr_code = data


    def reload_get_status(self):
        if self.api_token and self.instance_id and self.api_token:
            url = self.api_url + self.instance_id + "/status?token=" + self.api_token

            payload = {}
            headers = {}
            try:
                response = requests.request("GET", url, headers=headers, data=payload)
            except requests.exceptions.ConnectionError:
                raise UserError(
                    ("please check your internet connection."))
            if response.status_code == 200:
                dict = json.loads(response.text)
                if dict['accountStatus'] == 'authenticated':
                    self.authenticated = True
                    IrConfigParam = request.env['ir.config_parameter'].sudo()
                    base_url = IrConfigParam.get_param('web.base.url', False)
                    # x = base_url.split(":")
                    # http_tunnel = ngrok.connect(x[2]).public_url
                    data = {
                        "webhookUrl": base_url + "/webhook"
                    }
                    url = self.api_url + self.instance_id + "/webhook?token=" + self.api_token
                    headers = {'Content-type': 'application/json'}
                    try:
                        answer = requests.post(url, data=json.dumps(data), headers=headers)
                    except requests.exceptions.ConnectionError:
                        raise UserError(
                            ("please check your internet connection."))
                    if answer.status_code == 200:
                        dict = json.loads(answer.text)
                else:
                    x = dict['qrCode'].split(',')
                    message_bytes = x[1].encode('utf-8')
                    self.wa_qr_code =message_bytes

    def logout_account(self):
        if self.api_token and self.instance_id and self.api_token:
            url = self.api_url + self.instance_id + "/logout?token=" + self.api_token

            payload = {}
            headers = {}

            try:
                response = requests.request("POST", url, headers=headers, data=payload)
            except requests.exceptions.ConnectionError:
                raise UserError(
                    ("please check your internet connection."))
            if response.status_code == 200:
                dict = json.loads(response.text)
                self.authenticated = False
                self.wa_qr_code = False

