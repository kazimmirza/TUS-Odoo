from odoo import _, api, fields, models, modules, tools
import requests
import json
from odoo.exceptions import UserError, ValidationError

class ResPartner(models.Model):
    _inherit = 'res.partner'

    is_whatsapp_number = fields.Boolean('Is Whatsapp Number')

    def check_whatsapp_number(self):
        company_id = self.env.user.company_id.id
        company = self.env['res.company'].sudo().browse(company_id)
        if company.authenticated:
            url = company.api_url + company.instance_id + "/checkPhone?token=" + company.api_token + "&phone=+"+ self.phone.strip('+').replace(" ", "")

            payload = {}
            headers = {}

            try:
                response = requests.request("GET", url, headers=headers, data=payload)
            except requests.exceptions.ConnectionError:
                raise UserError(
                    ("please check your internet connection."))
            if response.status_code == 200:
                dict = json.loads(response.text)
                if 'result' in dict:
                    if dict['result'] == 'exists':
                        self.is_whatsapp_number = True
                    else:
                        raise UserError(
                            ("please check your whatsapp number."))
        else:
            raise UserError(
                ("please authenticated your whatsapp."))