from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    notification_time_out = fields.Char('Message Notification Time Out')
    users = fields.Many2one("res.users","User")

    def get_values(self):
        res = super(ResConfigSettings, self).get_values()

        res['notification_time_out'] = self.env['ir.config_parameter'].sudo().get_param('tus_wa_msgs.notification_time_out')
        res['users'] = self.env['ir.config_parameter'].sudo().get_param('tus_wa_msgs.users', default=False)
        res.update(
            {'users': int(res['users'])}
        )
        return res

    def set_values(self):
        self.env['ir.config_parameter'].sudo().set_param('tus_wa_msgs.notification_time_out', self.notification_time_out)
        self.env['ir.config_parameter'].sudo().set_param('tus_wa_msgs.users',(self.users.id or False))

        super(ResConfigSettings, self).set_values()
