from odoo import _, api, fields, models, tools

class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.model
    def create(self, values):
        user = super(ResUsers, self).create(values)
        if user.has_group('tus_wa_msgs.whats_app_group_user'):
            channels = self.env['mail.channel'].sudo().search([])
            for channel in channels:
                if channel.whatsapp_channel:
                    channel.write({'channel_partner_ids':[(4, user.partner_id.id)]})
        return user

    def write(self, vals):
        write_res = super(ResUsers, self).write(vals)
        if self.has_group('tus_wa_msgs.whats_app_group_user'):
            channels = self.env['mail.channel'].sudo().search([])
            for channel in channels:
                if channel.whatsapp_channel:
                    channel.write({'channel_partner_ids': [(4, self.partner_id.id)]})
        else:
            channels = self.env['mail.channel'].sudo().search([])
            for channel in channels:
                if channel.whatsapp_channel:
                    channel.write({'channel_partner_ids': [(3, self.partner_id.id)]})
        return write_res