from odoo import api, fields, models, _


class WATemplate(models.Model):
    _name = "wa.template"
    _inherit = ['mail.render.mixin']
    _description = 'Whatsapp Templates'

    @api.model
    def default_get(self, fields):
        res = super(WATemplate, self).default_get(fields)
        if not fields or 'model_id' in fields and not res.get('model_id') and res.get('model'):
            res['model_id'] = self.env['ir.model']._get(res['model']).id
        return res

    name = fields.Char('Name', translate=True)
    model_id = fields.Many2one(
        'ir.model', string='Applies to', required=True,
        # domain=['&', ('is_mail_thread_wa', '=', True), ('transient', '=', False)],
        help="The type of document this template can be used with", ondelete='cascade')
    model = fields.Char('Related Document Model', related='model_id.model', index=True, store=True, readonly=True)
    body_html = fields.Html('Body', translate=True, sanitize=False)

