from odoo import models, api, fields
import requests
import json
# from pyngrok import ngrok
from odoo.exceptions import UserError, ValidationError
import phonenumbers
import hashlib
import random
from phonenumbers.phonenumberutil import (
    region_code_for_country_code,
)

class WaMsgs(models.Model):
    _name = 'wa.msgs'

    name = fields.Char('Name')
    partner_id = fields.Many2one('res.partner','Partner')
    phn_no = fields.Char('Phone Number')
    message = fields.Char('Message')
    type = fields.Selection([
        ('in queue','In queue'),
        ('sent', 'Sent'),
        ('received', 'Received')],string='Type', default='received')
    attachment_ids = fields.Many2many(
        'ir.attachment', 'wa_message_attachment_rel',
        'wa_message_id', 'wa_attachment_id',
        string='Attachments', )

    def init(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        base_url = IrConfigParam.get_param('web.base.url', False)
        # x = base_url.split(":")
        # http_tunnel = ngrok.connect(x[2]).public_url
        data = {
            "webhookUrl": base_url + "/webhook"
        }
        company_id = self.env.user.company_id.id
        company = self.env['res.company'].sudo().browse(company_id)
        if company.authenticated:
            url = company.api_url + company.instance_id + "/webhook?token=" + company.api_token
            headers = {'Content-type': 'application/json'}
            try:
                response = requests.post(url, data=json.dumps(data), headers=headers)
            except requests.exceptions.ConnectionError:
                raise UserError(
                    ("please check your internet connection."))
            if response.status_code == 200:
                dict = json.loads(response.text)

    @api.onchange('phn_no')
    def change_partner(self):
        partner = self.env['res.partner'].search([('phone','=',self.phn_no)],limit=1)
        if partner:
            self.partner_id=partner.id
        else:
            self.partner_id=False

    @api.model
    def create(self, vals):
        partner_id = False
        channel_id = False
        partner = False
        if vals.get('partner_id') == False:
            pn = phonenumbers.parse('+' + vals.get('phn_no'))
            country_code = region_code_for_country_code(pn.country_code)
            country_id = self.env['res.country'].sudo().search(
                [('code', '=', country_code)], limit=1)
            partner = self.env['res.partner'].create(
                {'name': vals.get('phn_no'), 'phone': vals.get('phn_no'), 'country_id': country_id.id})
            partner_id = partner.id
            vals.update({'partner_id': partner_id})
        else:
            partner = self.env['res.partner'].sudo().browse(int(vals.get('partner_id')))
            partner_id = vals.get('partner_id')

        res = super(WaMsgs, self).create(vals)
        company_id = self.env.user.company_id.id
        company = self.env['res.company'].sudo().browse(company_id)
        if company.authenticated:
            # users = self.env['res.users'].sudo().search([])
            # part_lst = [user.partner_id.id for user in users if user.has_group('tus_wa_msgs.whats_app_group_user')]
            part_lst = []
            part_lst.append(int(vals.get('partner_id')))
            channel = False
            if vals.get('type') == 'received':
                channels = self.env['mail.channel'].sudo().search([])
                for chn in channels:
                    if int(vals.get('partner_id')) in chn.channel_partner_ids.ids and chn.whatsapp_channel:
                        channel = chn
                        break
                if channel:
                    message_values = {
                        'body': '<p> ' + vals.get('message') + '</p>',
                        'author_id': partner_id,
                        'email_from': partner.email or '',
                        'model': 'mail.channel',
                        'message_type': 'wa_msgs',
                        'isWaMsgs': True,
                        'subtype_id': self.env['ir.model.data'].sudo().xmlid_to_res_id('mail.mt_comment'),
                        'channel_ids': [(4, channel.id)],
                        'partner_ids': [(4, vals.get('partner_id'))],
                        'res_id': channel.id,
                        'reply_to': partner.email,
                    }
                    print("=vals.get('attachment_ids')vals.get('attachment_ids')==", vals.get('attachment_ids'))
                    if vals.get('attachment_ids'):
                        message_values.update({'attachment_ids': vals.get('attachment_ids')})
                    message = self.env['mail.message'].sudo().with_context({'message': 'received'}).create(
                        message_values)
                    notifications = channel._channel_message_notifications(message)
                    self.env['bus.bus'].sendmany(notifications)
                else:
                    name = vals.get('phn_no')
                    channel = self.env['mail.channel'].create({
                        'public': 'public',
                        'channel_type': 'chat',
                        'email_send': False,
                        'name': name,
                        'whatsapp_channel':True,
                        'channel_partner_ids': [(6, 0, part_lst)],
                    })
                    channel.write({'channel_last_seen_partner_ids': [(5, 0, 0)] + [(0, 0, {'partner_id': line_vals}) for
                                                                                   line_vals in part_lst]})

                    message_values = {
                        'body': '<p> ' + vals.get('message') + '</p>',
                        'author_id': partner_id,
                        'email_from': partner.email or '',
                        'model': 'mail.channel',
                        'message_type': 'wa_msgs',
                        'isWaMsgs': True,
                        'subtype_id': self.env['ir.model.data'].sudo().xmlid_to_res_id('mail.mt_comment'),
                        'channel_ids': [(4, channel.id)],
                        'partner_ids': [(4, vals.get('partner_id'))],
                        'res_id': channel.id,
                        'reply_to': partner.email,
                    }
                    print("=vals.get('attachment_ids')vals.get('attachment_ids')==", vals.get('attachment_ids'))
                    if vals.get('attachment_ids'):
                        message_values.update({'attachment_ids': vals.get('attachment_ids')})
                    message = self.env['mail.message'].sudo().with_context({'message': 'received'}).create(
                        message_values)

                    users = self.env['res.users'].search([])
                    for user in users:
                        if user.partner_id and user.partner_id.im_status == 'online' and user.has_group(
                                'tus_wa_msgs.whats_app_group_user'):
                            print("%s user is active. Sending notification." % user.name)
                            # Function call to prepare all the values required to display notification.
                            notif = self.get_next_notif(call_from=vals.get('phn_no'), user_id=user.id,message_id=message.id)
                            if notif:
                                notifications = [
                                    [(self._cr.dbname, 'incoming.Message.details', user.partner_id.id), notif]]
                                if notifications:
                                    self.env['bus.bus'].sendmany(notifications)
                    # notifications = channel._channel_message_notifications(message)
                    # self.env['bus.bus'].sendmany(notifications)


            else:
                if self.env.context.get('whatsapp'):
                    print("==self.env.context.get('whatsapp')======", self.env.context.get('whatsapp'))
                else:
                    if vals.get('message') != '':
                        data = {
                            "phone": vals.get('phn_no'),
                            "body": vals.get('message'),
                        }

                        url = company.api_url + company.instance_id + "/sendMessage?token=" + company.api_token
                        headers = {'Content-type': 'application/json'}
                        try:
                            answer = requests.post(url, data=json.dumps(data), headers=headers)
                        except requests.exceptions.ConnectionError:
                            raise UserError(
                                ("please check your internet connection."))

                    if len(vals.get('attachment_ids')) > 0:
                        for attc in vals.get('attachment_ids'):
                            file = self.env['ir.attachment'].browse(attc[1])
                            datas = file.datas.decode("utf-8")
                            # base64_encoded_data = base64.b64encode(file.datas)
                            # base64_message = base64_encoded_data.decode('utf-8')
                            data = {
                                "phone": vals.get('phn_no'),
                                "body": "data:" + file.mimetype + ";base64," + datas,
                                "filename": file.name,
                            }
                            url = company.api_url + company.instance_id + "/sendFile?token=" + company.api_token
                            headers = {'Content-type': 'application/json'}
                            try:
                                answer = requests.post(url, data=json.dumps(data), headers=headers)
                            except requests.exceptions.ConnectionError:
                                raise UserError(
                                    ("please check your internet connection."))
            return res
        else:
            raise UserError(
                ("please authenticated your whatsapp."))


    def get_next_notif(self, call_from=False, user_id=False,message_id=False):
        """
        function that search for the caller partner id and return the prepared dictionary.
        :return: dictionary of prepared details
        """
        message_string = ""
        partners = self.env['res.partner'].sudo().search([('phone', '=', call_from)], limit=1)
        print("==========",partners)
        # hashing to get unique value for image retrieval
        sha = hashlib.sha1(str(getattr(partners, '__last_update')).encode('utf-8')).hexdigest()[0:7]
        # Random number selector to keep divs of notifications for closing in all sessions
        notification = random.choice(range(10000,99999))
        if partners:
            # call.partner_id = partners.id
            message_string = """
            <div class="partner-details" data-partner-id='%s' id='%s'>
                <div class='row phone_no text-center ml-2'>Phone number :  %s</div>
                <div class='row text-center ml-2'>Partner Name :  %s</div>
                <img src='/web/image/res.partner/%s/image_128?unique=%s' height='90' width='90'/>
            </div>
            """ % (partners.id, notification, partners.phone, partners.name, partners.id, sha,)
            return {
                'message_id' : message_id,
                'user_id': user_id,
                'partner_id': partners.id if partners else False,
                'message': message_string,
                'notification': notification,
                'title': "Message Info",
                'sticky': True,
                'type': 'info',
                'call_from': call_from,
            }
        return False

    def addPartnerInChannel(self,partner_id,notification,notification_id):
        channels = self.env['mail.channel'].sudo().search([])
        print("===============",partner_id,notification)

        if notification:
            users = self.env['res.users'].search([])
            for user in users:
                if user.partner_id and user.partner_id.im_status == 'online' and user.has_group('tus_wa_msgs.whats_app_group_user'):
                    notifications = [[(self._cr.dbname, 'close.incoming.Message.details', user.partner_id.id), {'notification_id': notification_id}]]
                    if notifications:
                        self.env['bus.bus'].sendmany(notifications)
        for chn in channels:
            if int(partner_id) in chn.channel_partner_ids.ids and chn.whatsapp_channel:
                if chn.whatsapp_channel:
                    chn.write({'channel_partner_ids': [(4, self.env.user.partner_id.id)]})
                    mail_channel_partner = self.env['mail.channel.partner'].sudo().search(
                        [('channel_id', '=', chn.id), ('partner_id', '=', self.env.user.partner_id.id)])
                    mail_channel_partner.write({'is_pinned': True})
                    message = self.env['mail.message'].sudo().browse(notification['message_id'])
                    notifications = chn._channel_message_notifications(message)
                    self.env['bus.bus'].sendmany(notifications)
                    # chn.write({'channel_last_seen_partner_ids':  [(4, self.env.user.partner_id.id)]})
        return True

    def addDefaultPartnerChannel(self,partner_id,notification,notification_id):
        channels = self.env['mail.channel'].sudo().search([])
        print("=============hhhhhhhhhhhh==", partner_id, notification)
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        user_id = IrConfigParam.get_param('tus_wa_msgs.users', False)
        print("===========",user_id)

        if notification:
            users = self.env['res.users'].search([])
            for user in users:
                if user.partner_id and user.partner_id.im_status == 'online' and user.has_group(
                        'tus_wa_msgs.whats_app_group_user'):
                    notifications = [[(self._cr.dbname, 'close.incoming.Message.details', user.partner_id.id),
                                      {'notification_id': notification_id}]]
                    if notifications:
                        self.env['bus.bus'].sendmany(notifications)
        if user_id:
            partner= self.env['res.users'].sudo().browse(int(user_id)).partner_id.id
            for chn in channels:
                if int(partner_id) in chn.channel_partner_ids.ids and chn.whatsapp_channel:
                    if chn.whatsapp_channel:
                        chn.write({'channel_partner_ids': [(4, partner)]})
                        mail_channel_partner = self.env['mail.channel.partner'].sudo().search(
                            [('channel_id', '=', chn.id), ('partner_id', '=', partner)])
                        mail_channel_partner.write({'is_pinned': True})
                        message = self.env['mail.message'].sudo().browse(notification['message_id'])
                        notifications = chn._channel_message_notifications(message)
                        self.env['bus.bus'].sendmany(notifications)
                        # chn.write({'channel_last_seen_partner_ids':  [(4, self.env.user.partner_id.id)]})
        return True
