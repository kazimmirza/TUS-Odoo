{
    'name': 'Whatsapp Message',
    'version': '1.0.',
    'author': 'Techultra Solutions',
    'website': 'www.techultrasolutions.com',
    'description': """

    """,
    'depends': ['base','mail','purchase','sale_management','point_of_sale'],
    'data': [
        'security/ir.model.access.csv',
        'security/wa_msgs_security.xml',
        'wizard/wa_compose_message_view.xml',
        'views/assets.xml',
        'views/wa_mesgs_views.xml',
        'views/res_config_settings_views.xml',
        'views/mail_channel_views.xml',
        'views/mail_message_views.xml',
        'views/res_company_views.xml',
        'views/sale_order_views.xml',
        'views/purchase_order_views.xml',
        'views/res_partner_view.xml',
        'views/wa_template_views.xml',
    ],
    'qweb': [
             'static/src/components/chatter_topbar/discuss_ext.xml',
             'static/src/components/chatter_topbar/message_ext.xml',
             'static/src/components/chatter_topbar/chat_window.xml',
             'static/src/components/ReceiptScreen/ReceiptScreen.xml',
             'static/src/components/popup/pos_wa_composer.xml',
             ],
    'installable': True,
    'auto_install': False,
    'license': 'LGPL-3'
}