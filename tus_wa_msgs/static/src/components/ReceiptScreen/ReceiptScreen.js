odoo.define('tus_wa_msgs.ReceiptScreen', function(require) {
    'use strict';

    const ReceiptScreen = require('point_of_sale.ReceiptScreen');
    const Registries = require('point_of_sale.Registries');
    const { Printer } = require('point_of_sale.Printer');
    const ajax = require('web.ajax');
    var models = require('point_of_sale.models');

    models.load_models({
        model: 'wa.template',
        fields: ['id','name' ,'model_id'],
        domain: function(self){ return [['model_id.model','=','pos.order']]; },
        loaded: function(self,templates){
            self.templates = templates;
        },
    });

    const PosResReceiptScreen = ReceiptScreen =>
        class extends ReceiptScreen {
            /**
             * @override
             */
               mounted() {
                    // Here, we send a task to the event loop that handles
                    // the printing of the receipt when the component is mounted.
                    // We are doing this because we want the receipt screen to be
                    // displayed regardless of what happen to the handleAutoPrint
                    // call.
                    setTimeout(async () => await this.handleAutoPrint(), 0);
                    var self = this
                    this.env.session.user_has_group('tus_wa_msgs.whats_app_group_user').then(function(has_group){
                        if(has_group){
                             $(self.el).find('.send_by_whatsapp').show()
                        } else {
                            $(self.el).find('.send_by_whatsapp').hide()
                        }
                    });
               }
               async sendByWhatsapp(){
                    var def = new $.Deferred();
                    var self = this;
                    this.showPopup('WaComposerPopup', {
                        transaction: def,
                    });
                    $(document).ready(function () {
                        const order = self.currentOrder;
                        const client = order.get_client();
                        const phone = client.phone
                        $('#phone').val(phone);
                        $('.wa_template').change(function(){
                            const template_id = $( ".wa_template option:selected" ).val();
                            const orderName = order.get_name();
                            ajax.jsonRpc("/get/template/content", 'call', {
                                                'template_id':template_id,
                                                'order_name':orderName,
                                            }).then(function (data) {
                                                if(data){
                                                    $('#message').val(data)
                                                }
                                                else{
                                                    $('#message').val('')
                                                }
                                            });
                        })

                        $('.sendMessage').click(async function(){
                            const printer = new Printer();
                            const receiptString = self.orderReceipt.comp.el.outerHTML;
                            const ticketImage = await printer.htmlToImg(receiptString);
                            const order = self.currentOrder;
                            const orderName = order.get_name();
                            ajax.jsonRpc("/send/receipt", 'call', {
                                        'message':$('#message').val(),
                                        'image':ticketImage,
                                        'phone' : phone,
                                        'id': client.id,
                                        'receipt_name':orderName,
                                    }).then(function (data) {
                                        $('.wa_cancel').click();
                                    });
                                    })
                    });
                }
        };

    Registries.Component.extend(ReceiptScreen, PosResReceiptScreen);

    return ReceiptScreen;
});
