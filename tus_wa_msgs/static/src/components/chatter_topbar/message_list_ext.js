odoo.define('tus_wa_msgs/static/src/components/chatter_topbar/message_list_ext.js', function (require) {
//"use strict";



//odoo.define('website_slides/static/src/components/activity/activity.js', function (require) {
//'use strict';
const StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
const components = {
    discuss: require('mail/static/src/components/discuss/discuss.js'),
    discuss_sidebar_item: require('mail/static/src/components/discuss_sidebar_item/discuss_sidebar_item.js'),
    thread_view: require('mail/static/src/components/thread_view/thread_view.js'),
    chat_window: require('mail/static/src/components/chat_window/chat_window.js'),
};
const useRefs = require('mail/static/src/component_hooks/use_refs/use_refs.js');
const useStore = require('mail/static/src/component_hooks/use_store/use_store.js');
const useUpdate = require('mail/static/src/component_hooks/use_update/use_update.js');

//const message_list_old =  require('mail/static/src/components/message_list/message_list.js')
const { Component, tags, hooks} = owl;
const { useState } = hooks;

const { registerInstancePatchModel } = require('mail/static/src/model/model_core.js');
const Composer = require("mail/static/src/components/composer/composer.js");
const { clear } = require('mail/static/src/model/model_field_command.js');

const { patch } = require('web.utils');
const {
	    addLink,
	    escapeAndCompactTextContent,
	    parseAndTransform,
	} = require('mail.utils');
const session = require('web.session');
const ajax = require('web.ajax');

registerInstancePatchModel('mail.thread', 'mail/static/src/models/thread/thread.js', {
     _computeLocalMessageUnreadCounter() {
            if (this.model !== 'mail.channel') {
                // unread counter only makes sense on channels
                return clear();
            }
            // By default trust the server up to the last message it used
            // because it's not possible to do better.
            let baseCounter = this.serverMessageUnreadCounter;
            let countFromId = this.serverLastMessageId;
            // But if the client knows the last seen message that the server
            // returned (and by assumption all the messages that come after),
            // the counter can be computed fully locally, ignoring potentially
            // obsolete values from the server.
            const firstMessage = this.orderedMessages[0];
            if (
                firstMessage &&
                this.lastSeenByCurrentPartnerMessageId &&
                this.lastSeenByCurrentPartnerMessageId >= firstMessage.id
            ) {
                baseCounter = 0;
                countFromId = this.lastSeenByCurrentPartnerMessageId;
            }
            // Include all the messages that are known locally but the server
            // didn't take into account.
            var hh = this.orderedMessages.reduce((total, message) => {
                if (message.id <= countFromId) {
                    return total;
                }
                return total + 1;
            }, baseCounter)
            var lst = []
            const myArr = Array.from(this.__values.messages)
            var lst2 = myArr.sort(function(first, second) {
                return second.__values.id - first.__values.id;
            });

            for(var i=0;i<hh;i++){
                if(lst2[i]){
                    lst.push(lst2[i])
                }
            }
            const aeee1  = Array.from(lst)
            const aeee2  = Array.from(lst)
            var wa_messages = []
            var li_messages = []

            if(aeee1.length > 0){
                wa_messages = aeee1.filter(mes => mes.__values.message_type == 'wa_msgs');
                li_messages = aeee2.filter(mes => mes.__values.message_type == 'comment');
            }
            var waUnreadCounter = 0
            var liveUnreadCounter = 0
            if(wa_messages.length > 0){
                 waUnreadCounter = wa_messages.length
            }
            if(li_messages.length > 0){
                liveUnreadCounter = li_messages.length
            }
            if(hh > 0){
                this.waUnreadCounter = waUnreadCounter
                this.liveUnreadCounter = liveUnreadCounter
            }
            if(hh == 0 && this.waUnreadCounter > 0 && this.liveUnreadCounter > 0){
                hh = this.waUnreadCounter + this.liveUnreadCounter
            }
            else if(hh == 0 && this.waUnreadCounter > 0 ){
                hh = this.waUnreadCounter
            }
            else if(hh == 0 && this.liveUnreadCounter > 0 ){
                hh = this.liveUnreadCounter
            }
            else{
                hh = hh
            }
            return hh
//            return this.orderedMessages.reduce((total, message) => {
//                if (message.id <= countFromId) {
//                    return total;
//                }
//                return total + 1;
//            }, baseCounter);
        },


});

registerInstancePatchModel('mail.composer', 'mail/static/src/models/composer/composer.js', {
    async postMessage() {
            const thread = this.thread;
            this.thread.unregisterCurrentPartnerIsTyping({ immediateNotify: true });
            const escapedAndCompactContent = escapeAndCompactTextContent(this.textInputContent);
            let body = escapedAndCompactContent.replace(/&nbsp;/g, ' ').trim();
            // This message will be received from the mail composer as html content
            // subtype but the urls will not be linkified. If the mail composer
            // takes the responsibility to linkify the urls we end up with double
            // linkification a bit everywhere. Ideally we want to keep the content
            // as text internally and only make html enrichment at display time but
            // the current design makes this quite hard to do.
            body = this._generateMentionsLinks(body);
            body = parseAndTransform(body, addLink);
            body = this._generateEmojisOnHtml(body);
            let postData;
            if(thread.threadViews[0].wa_msgs){
                postData = {
                    attachment_ids: this.attachments.map(attachment => attachment.id),
                    body,
                    channel_ids: this.mentionedChannels.map(channel => channel.id),
                    message_type: 'wa_msgs',
                    partner_ids: this.recipients.map(partner => partner.id),
                };
            }
            else{
                postData = {
                    attachment_ids: this.attachments.map(attachment => attachment.id),
                    body,
                    channel_ids: this.mentionedChannels.map(channel => channel.id),
                    message_type: 'comment',
                    partner_ids: this.recipients.map(partner => partner.id),
                };
            }
            if (this.subjectContent) {
                postData.subject = this.subjectContent;
            }
            try {
                let messageId;
                this.update({ isPostingMessage: true });
                if (thread.model === 'mail.channel') {
                    const command = this._getCommandFromText(body);
                    Object.assign(postData, {
                        subtype_xmlid: 'mail.mt_comment',
                    });
                    if (command) {
                        messageId = await this.async(() => this.env.models['mail.thread'].performRpcExecuteCommand({
                            channelId: thread.id,
                            command: command.name,
                            postData,
                        }));
                    } else {
                        messageId = await this.async(() =>
                            this.env.models['mail.thread'].performRpcMessagePost({
                                postData,
                                threadId: thread.id,
                                threadModel: thread.model,
                            })
                        );
                    }
                } else {
                    Object.assign(postData, {
                        subtype_xmlid: this.isLog ? 'mail.mt_note' : 'mail.mt_comment',
                    });
                    if (!this.isLog) {
                        postData.context = {
                            mail_post_autofollow: true,
                        };
                    }
                    messageId = await this.async(() =>
                        this.env.models['mail.thread'].performRpcMessagePost({
                            postData,
                            threadId: thread.id,
                            threadModel: thread.model,
                        })
                    );
                    const [messageData] = await this.async(() => this.env.services.rpc({
                        model: 'mail.message',
                        method: 'message_format',
                        args: [[messageId]],
                    }, { shadow: true }));
                    this.env.models['mail.message'].insert(Object.assign(
                        {},
                        this.env.models['mail.message'].convertData(messageData),
                        {
                            originThread: [['insert', {
                                id: thread.id,
                                model: thread.model,
                            }]],
                        })
                    );
                    thread.loadNewMessages();
                }
                for (const threadView of this.thread.threadViews) {
                    // Reset auto scroll to be able to see the newly posted message.
                    threadView.update({ hasAutoScrollOnMessageReceived: true });
                }
                thread.refreshFollowers();
                thread.fetchAndUpdateSuggestedRecipients();
                this._reset();
            } finally {
                this.update({ isPostingMessage: false });
            }
        },

});



patch(components.chat_window, 'tus_wa_msgs/static/src/components/chatter_topbar/message_list_ext.js', {
     mounted() {
        console.log("sssssssssssssssssssss")
        this.env.messagingBus.on('will_hide_home_menu', this, this._onWillHideHomeMenu);
        this.env.messagingBus.on('will_show_home_menu', this, this._onWillShowHomeMenu);
        var self = this
        this.env.session.user_has_group('tus_wa_msgs.whats_app_group_user').then(function(has_group){
            if(has_group){

                 if(self.chatWindow.thread && self.chatWindow.thread.__values.correspondent){
                    ajax.jsonRpc("/check/partner/user", 'call', {
                                        'partner_id':self.chatWindow.thread.__values.correspondent.__values.id,
                                    },{'async':false}).then(function (data) {
                                       if(data){
                                            $(self.el).find('.live_chatbox_tab').show();
                                       } else {
                                            $(self.el).find('.live_chatbox_tab').hide();
                                            $(self.el).find('.live_chatbox_tab').removeClass('active');
                                            $(self.el).find('.whatsup_chatbox_tab').addClass('active');
                                            $(self.el).find('.live_chat').removeClass('current');
                                            $(self.el).find('.whatsup_chat').addClass('current');
                                       }
                                    });
                    self.wa_msgs_btn();
                }
            } else {
                $(self.el).find('.chat_box_tab').hide()
                 $(self.el).find('.whatsup_chat_tab').removeClass('active')
                 $(self.el).find('.live_chat_tab').addClass('active')
                 self._onLive();
            }
        });
    },

     _onLive() {
        $(this.el).addClass('live')
        $(this.el).removeClass('whatsapp')
        $(this.el).find('.whatsup_chatbox_tab').removeClass('active')
        $(this.el).find('.live_chatbox_tab').addClass('active')
        $(this.el).find('.o_ThreadView_messageList').css({ "background-image": "none",});
        this.chatWindow.__values.threadView.wa_msgs = false
        this.chatWindow.__values.thread.open()
    },
    /**
     * @private
     */
    wa_msgs_btn() {
        var url = session.url('/web/image', {model: 'res.company', id: session.company_id, field: 'back_image',});
        $(this.el).addClass('whatsapp')
        $(this.el).removeClass('live')
        $(this.el).find('.whatsup_chatbox_tab').addClass('active')
        $(this.el).find('.live_chatbox_tab').removeClass('active')
        if(this.chatWindow.__values.threadView){
            this.chatWindow.__values.threadView.wa_msgs = true
            this.chatWindow.__values.thread.open()
        }
        var self=this
        setTimeout(function() {
            $(self.el).find('.o_ThreadView_messageList').css({
                "background-image": "url(" + url + ")",
                "background-size": "cover",
            });
        },100);
    },

    _onClickedHeader(ev) {
        ev.stopPropagation();
        if (this.env.messaging.device.isMobile) {
            return;
        }
        if (this.chatWindow.isFolded) {
            this.chatWindow.unfold();
            this.chatWindow.focus();
            this.wa_msgs_btn()
        } else {
            this._saveThreadScrollTop();
            this.chatWindow.fold();
        }
    },
});

patch(components.thread_view, 'tus_wa_msgs/static/src/components/chatter_topbar/message_list_ext.js', {
     _useStoreSelector(props) {
        const threadView = this.env.models['mail.thread_view'].get(props.threadViewLocalId);
        const thread = threadView ? threadView.thread : undefined;
        const threadCache = threadView ? threadView.threadCache : undefined;
        if(thread){
            const myArr = Array.from(thread.__values.orderedMessages)
            if(threadView.wa_msgs){
                var messages = myArr.filter(mes => mes.__values.message_type == 'wa_msgs');
                const mySet2 = new Set(messages)
                threadCache.__values.orderedMessages = mySet2
            }
            else{
                var messages = myArr.filter(mes => mes.__values.message_type == 'comment');
                const mySet2 = new Set(messages)
                threadCache.__values.orderedMessages = mySet2
            }
        }
        return {
            isDeviceMobile: this.env.messaging.device.isMobile,
            thread: thread ? thread.__state : undefined,
            threadCache: threadCache ? threadCache.__state : undefined,
            threadView: threadView ? threadView.__state : undefined,
        };
    },
});


patch(components.discuss, 'tus_wa_msgs/static/src/components/chatter_topbar/message_list_ext.js', {

    //--------------------------------------------------------------------------
    // Handlers
    //--------------------------------------------------------------------------

    /**
     * @private

     */
    mounted() {
        this.discuss.update({ isOpen: true });
        if (this.discuss.thread) {
            this.trigger('o-push-state-action-manager');
        } else if (this.env.messaging.isInitialized) {
            this.discuss.openInitThread();
        }
        this._updateLocalStoreProps();
        var self =this
        this.env.session.user_has_group('tus_wa_msgs.whats_app_group_user').then(function(has_group){
            if(has_group){
                 self.wa_msgs_btn();
            } else {
                $(self.el).find('.whatsup_chat_tab').hide()
                $(self.el).find('.whatsup_chat_tab').removeClass('active')
                $(self.el).find('.live_chat_tab').addClass('active')
                self._onLive();
            }
        });
        this._onChatTab()
    },

    _onLive() {
        $('.o_ComposerTextInput_textarea').addClass('live')
        $('.o_ComposerTextInput_textarea').removeClass('whatsapp')
        $(this.el).find('.o_ThreadView_messageList').css({"background-image": "none", });
        if(this.discuss.thread){
            this.discuss.thread.liveUnreadCounter = 0
            if(this.discuss.thread.waUnreadCounter > 0){
                this.discuss.thread.__values.localMessageUnreadCounter = this.discuss.thread.waUnreadCounter
             }
             else{
                this.discuss.thread.__values.localMessageUnreadCounter = 0
             }
            this.discuss.threadView.wa_msgs = false
            this.discuss.__values.thread.open()
            this._onChatTab()
        }
    },
    /**
     * @private
     */
    wa_msgs_btn() {
        if(this.discuss.threadModel == 'mail.channel'){
            var url = session.url('/web/image', {model: 'res.company', id: session.company_id, field: 'back_image',});
            $(this.el).find('.o_ThreadView_messageList').css({
                "background-image": "url(" + url + ")",
                "background-size": "cover",
            });
        }
        else{
            $(this.el).find('.o_ThreadView_messageList').css({ "background-image": "none",});
        }
        $('.o_ComposerTextInput_textarea').addClass('whatsapp')
        $('.o_ComposerTextInput_textarea').removeClass('live')
        if(this.discuss.__values.threadView){
            this.discuss.__values.threadView.wa_msgs = true
            this.discuss.__values.thread.waUnreadCounter = 0
            if(this.discuss.thread.liveUnreadCounter > 0){
                this.discuss.thread.__values.localMessageUnreadCounter = this.discuss.thread.liveUnreadCounter
            } else {
                this.discuss.thread.__values.localMessageUnreadCounter = 0
            }
            this.discuss.__values.thread.open()
            this._onChatTab()
        }
    },

     _onChatTab(){
        var self = this;
        var tabsNewAnim = $(self.el).find('#navbarSupportedContent');
        var selectorNewAnim = $(self.el).find('#navbarSupportedContent').find('li').length;
        var activeItemNewAnim = tabsNewAnim.find('.active');
        var activeWidthNewAnimHeight = activeItemNewAnim.innerHeight();
        var activeWidthNewAnimWidth = activeItemNewAnim.innerWidth();
        var itemPosNewAnimTop = activeItemNewAnim.position();
        var itemPosNewAnimLeft = activeItemNewAnim.position();
        if(itemPosNewAnimTop){
            $(self.el).find(".hori-selector").css({
              "top":itemPosNewAnimTop.top + "px",
              "left":itemPosNewAnimLeft.left + "px",
              "height": activeWidthNewAnimHeight + "px",
              "width": activeWidthNewAnimWidth + "px"
            });
        }
    },

     _onThreadRendered(ev) {
        this.trigger('o-update-control-panel');
        var self = this
        $(self.el).find('.live_chat_tab').show();
        if(self.discuss.threadModel != 'mail.channel'){
            $(self.el).find('.navbar').addClass('o_hidden');
            $(self.el).find('.o_ThreadView_messageList').css({ "background-image": "none",});
        }
        else{
            if(this.discuss.__values.threadView.wa_msgs){
                var url = session.url('/web/image', {model: 'res.company', id: session.company_id, field: 'back_image',});
                $(self.el).find('.o_ThreadView_messageList').css({
                    "background-image": "url(" + url + ")",
                    "background-size": "cover",
                });
            }
             $(self.el).find('.navbar').removeClass('o_hidden');
        }
        if(self.discuss.thread && self.discuss.thread.__values.correspondent){
            ajax.jsonRpc("/check/partner/user", 'call', {
                                'partner_id':self.discuss.thread.__values.correspondent.__values.id,
                            },{'async':false}).then(function (data) {
                               if(data){
                                    $(self.el).find('.live_chat_tab').show();
                               }
                               else{
                                    $(self.el).find('.live_chat_tab').hide();
                                    self.discuss.thread.liveUnreadCounter = 0
                                    $(self.el).find('.live_chat_tab').removeClass('active');
                                    $(self.el).find('.whatsup_chat_tab').addClass('active');
                                    $(self.el).find('.live_chat').removeClass('current');
                                    $(self.el).find('.whatsup_chat').addClass('current');
                                    if(!$(self.el).find('.o_ComposerTextInput_textarea').hasClass('whatsapp')){
                                    self.wa_msgs_btn();
                                    }
                               }
                            });
        }
        this._onChatTab()
    },
});
});



