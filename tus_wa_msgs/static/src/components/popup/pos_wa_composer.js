odoo.define('tus_wa_msgs.WaComposerPopup', function(require) {
    'use strict';

    const { useState } = owl.hooks;
    const AbstractAwaitablePopup = require('point_of_sale.AbstractAwaitablePopup');
    const Registries = require('point_of_sale.Registries');

    class WaComposerPopup extends AbstractAwaitablePopup {
        constructor() {
            super(...arguments);
        }
    }
    WaComposerPopup.template = 'WaComposerPopup';
    WaComposerPopup.defaultProps = {
        confirmText: 'Ok',
        cancelText: 'Cancel',
        body: '',
    };

    Registries.Component.add(WaComposerPopup);

    return WaComposerPopup;
});