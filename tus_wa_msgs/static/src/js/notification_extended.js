odoo.define('tus_wa_msgs.caller_notification', function (require) {
"use strict";
    var Notification = require('web.Notification');
    var WebClient = require('web.WebClient');
    var ajax = require('web.ajax');

    Notification.include({
    _onClickButton: function (ev) {
        ev.preventDefault();
        if (this._buttonClicked) {
            return;
        }
        this._buttonClicked = true;
        var index = $(ev.currentTarget).index();
        var button = this.buttons[index];
        if (button.click) {
            button.click(ev);
        }
        this.close(true);
    },
})

    WebClient.include({
    display_caller_notif: function(notifications) {
        var self = this;
        self.accept = false
        var timeout = 5000
        ajax.rpc('/web/dataset/search_read', {
                model: 'ir.config_parameter',
                domain: [['key','in',['tus_wa_msgs.notification_time_out']]],
            }).then(function (results) {
                if (results.length) {

                    timeout = parseInt(results.records[0].value)
                    console.log("==========",timeout)
                }
            });

        var notif_id = self.call('notification', 'notify', {
            title: notifications.title,
            message: notifications.message,
            notification: notifications.notification,
            partnerID: notifications.partner_id,
            messageID: notifications.message_id,
            userID: notifications.user_id,
            type: notifications.type,
            buttons: [{
                click: function(ev){
                    console.log("==========",ev)
                    self.accept = true
                	var partner_id = parseInt($(ev.currentTarget).closest('.o_notification').find('div.partner-details').data('partner-id'))
                    self._rpc({
                        model: 'wa.msgs',
                        method: 'addPartnerInChannel',
                        args: [[], partner_id,notifications,parseInt($(ev.currentTarget).closest('.o_notification').find('div.partner-details').data('notif-id'))],
                    });
                },
                primary: true,
                text: 'Accept',
            }],
        });
        setTimeout(function() {
                console.log("======================default addddd partner kkkk",self.accept,notifications,notif_id)
                if(self.accept == false){
                     self._rpc({
                        model: 'wa.msgs',
                        method: 'addDefaultPartnerChannel',
                        args: [[], notifications.partner_id,notifications,parseInt(notif_id)],
                    }).then(function(accept){
                        if(accept){
                            self.accept = true
                        }
                    });
                }
//                $(self.el).find('#'+String(notifications.notification)).attr('data-notif-id', notif_id);
        }, timeout);
        /* Setting timeout as calling notification > notify and need to attach notification id to notification */
        setTimeout(function() {
                $(self.el).find('#'+String(notifications.notification)).attr('data-notif-id', notif_id);
        }, 1500);

    },

    /* Sending rpc call to close popups when any user clicks on profile button. */
//    send_rpc: function(notifications, notification_id){
//        console.log("======",notifications,notification_id)
//        return this._rpc({
//            model: 'cx.xtrnl.phone.call',
//            method: 'close_call_notification',
//            args: [[], notifications, notification_id],
//        });
//    },

    /* Close all popup windows in all user sessions. */
    close_caller_notif: function(notifications) {
        console.log("=============================close11111")
        var self = this;
        self.call("notification", "close", notifications.notification_id);
    },

    show_application: function() {
        // An event is triggered on the bus each time a caller notification is arrived
        var self = this;
        this.call('bus_service', 'onNotification', this, function (notifications) {
            _.each(notifications, (function (notification) {
                if (notification[0][1] === 'incoming.Message.details') {
                    this.display_caller_notif(notification[1]);
                }
                if (notification[0][1] === 'close.incoming.Message.details'){
                    this.close_caller_notif(notification[1]);
                }
            }).bind(this));
        });
        return this._super.apply(this, arguments);
    },
});

});